package single.ru.ex.UI.controllers.main;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import ru.yandex.yandexmapkit.utils.GeoPoint;
import single.ru.ex.R;
import single.ru.ex.UI.controllers.list_items.ItemFragment;
import single.ru.ex.UI.controllers.map.MapFragment;
import single.ru.ex.UI.data.DataPointsContent;


public class MainActivity extends ActionBarActivity implements OnFragmentInteractionListener{

    private DataPointsContent mData = new DataPointsContent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemSelected(int position) {
        //CreateAndShowAD("Not Implemented", "Here locate to ballon and show bubble info");

        final MapFragment mf = (MapFragment) getFragmentManager().findFragmentById(R.id.fragment_map);
        if (mf != null) {
            final String id = mData.getData().get(position).id;

            View v = mf.getView();
            if (v != null) {
                v.post(new Runnable() {
                    @Override
                    public void run() {
                        mf.onItemSelected(id);
                    }
                });
            }
        }
    }


    @Override
    public void onBalloonAdded(GeoPoint geopoint, final String[] id, final String[] text) {
        //TODO: Add dialog with description
        DataPointsContent.DataItem bi = mData.addItem(geopoint, "Input an description if needed");
        if ((id == null || id.length <= 1) && (text == null || text.length <= 1)){
            id[0] = bi.id;
            text[0] = bi.toString();
        }


        final ItemFragment itf = (ItemFragment) getFragmentManager().findFragmentById(R.id.fragment_list);
        if (itf != null) {
            View v = itf.getView();
            if (v != null) {
                v.post(new Runnable() {
                    @Override
                    public void run() {
                        itf.itemsUpdated(mData.getData());
                    }
                });
            }
        }
    }

    @Override
    public void onYaNavActivated(int pos) {
        if (pos >= 0 && pos < mData.getData().size()) {
            // Создаем интент для построения маршрута
            Intent intent = new Intent("ru.yandex.yandexnavi.action.BUILD_ROUTE_ON_MAP");
            intent.setPackage("ru.yandex.yandexnavi");

            PackageManager pm = getPackageManager();
            List<ResolveInfo> infos = pm.queryIntentActivities(intent, 0);

            // Проверяем, установлен ли Яндекс.Навигатор
            if (infos == null || infos.size() == 0) {
                // Если нет - будем открывать страничку Навигатора в Google Play
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=ru.yandex.yandexnavi"));
            } else {

                GeoPoint gp = mData.getData().get(pos).geopos;
                intent.putExtra("lat_to", gp.getLat());
                intent.putExtra("lon_to", gp.getLon());
            }

            // Запускаем нужную Activity
            startActivity(intent);
        } else {
            CreateAndShowAD("Error", "Something went wrong!");
        }
    }

    public void CreateAndShowAD(String title, String msg) {
        AlertDialog dialog;
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(title);
        adb.setMessage(msg);
        dialog = adb.create();
        dialog.show();
    }
}
