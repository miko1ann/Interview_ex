package single.ru.ex.UI.data;

import java.util.ArrayList;
import java.util.List;

import ru.yandex.yandexmapkit.utils.GeoPoint;

public class DataPointsContent {

    private static List<DataItem> ITEMS = new ArrayList<>();

    public DataItem addItem(GeoPoint gp, String content) {
        DataItem bi = new DataItem(content, gp);
        ITEMS.add(bi);
        return bi;
    }

    public List<DataItem> getData() {
        return ITEMS;
    }



    public static class DataItem {
        private static int counter = 0;

        public final String id;
        public String content;
        public GeoPoint geopos;


        public DataItem(String content, GeoPoint geopos) {
            this.id = String.valueOf(counter++);
            this.content = content;
            this.geopos = geopos;
        }


        @Override
        public String toString() {
            return id + " " + content + " " + geopos.toString();
        }
    }

}
