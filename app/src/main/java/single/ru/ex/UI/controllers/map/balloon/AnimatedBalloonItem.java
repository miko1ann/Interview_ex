package single.ru.ex.UI.controllers.map.balloon;

import android.content.Context;
import android.view.View;

import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;
import ru.yandex.yandexmapkit.utils.GeoPoint;

/**
 * Created by Apple on 08.09.15.
 */
public class AnimatedBalloonItem extends BalloonItem {

    public AnimatedBalloonItem(Context context, GeoPoint geoPoint) {
        super(context, geoPoint);
    }

    @Override
    public void inflateView(Context context){
        super.inflateView(context);
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }

   public View getView(){
       return model;
   }
}
