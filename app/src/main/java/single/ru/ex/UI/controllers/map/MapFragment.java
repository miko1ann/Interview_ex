package single.ru.ex.UI.controllers.map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.map.GeoCode;
import ru.yandex.yandexmapkit.map.GeoCodeListener;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;
import ru.yandex.yandexmapkit.overlay.balloon.OnBalloonListener;
import single.ru.ex.R;
import single.ru.ex.UI.controllers.main.OnFragmentInteractionListener;
import single.ru.ex.UI.controllers.map.balloon.AnimatedBalloonItem;


public class MapFragment extends Fragment{

    private OnFragmentInteractionListener mListener;

    private MapView mMapView = null;
    private MapController mMapController = null;
    private OverlayManager mOverlayManager = null;
    private CustomOverlay mCustomOverlay = null;

    private GeoCodeListener mGCListener = new GeoCodeListener() {

        @Override
        public boolean onFinishGeoCode(final GeoCode geopos) {
            boolean result = true;

            if (geopos != null && mCustomOverlay != null) {
                Resources res = getResources();

                OverlayItem overItem = new OverlayItem(geopos.getGeoPoint(), res.getDrawable(R.drawable.balloon_red));
                AnimatedBalloonItem balloon = new AnimatedBalloonItem(mMapView.getContext(), overItem.getGeoPoint());
                overItem.setBalloonItem(balloon);
                balloon.setOnBalloonListener(mBalloonListener);

                if (mListener != null) {
                    String[] id = new String[1];
                    String[] text = new String[1];
                    mListener.onBalloonAdded(geopos.getGeoPoint(), id, text);

                    if (id != null && id[0] != null && !id[0].isEmpty()) {
                        mCustomOverlay.addOverlayItem(id[0], overItem);
                    } else {
                        result = false;
                    }

                    if (text != null && text[0] != null && !text[0].isEmpty()) {
                        balloon.setText(text[0]);
                    } else {
                        result = false;
                    }

                    mMapView.getMapController().notifyRepaint();
                }

            } else {
                result = false;
            }
            if (!result) {
                mMapController.getMapView().post(new Runnable() {
                    @Override
                    public void run() {
                        String title = "Something wrong =(";
                        String msg = "The problem during performing GeoPosition";
                        AlertDialog dialog;
                        AlertDialog.Builder adb = new AlertDialog.Builder(mMapView.getContext());
                        adb.setTitle(title);
                        adb.setMessage(msg);
                        dialog = adb.create();
                        dialog.show();
                    }
                });
            }

            return true;
        }
    };

    private OnBalloonListener mBalloonListener = new OnBalloonListener() {
        @Override
        public void onBalloonViewClick(BalloonItem balloonItem, View view) {

        }

        @Override
        public void onBalloonShow(BalloonItem balloonItem) {

        }

        @Override
        public void onBalloonHide(BalloonItem balloonItem) {

        }

        @Override
        public void onBalloonAnimationStart(BalloonItem balloonItem) {


        }

        @Override
        public void onBalloonAnimationEnd(BalloonItem balloonItem) {

        }
    };

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_map, container, false);
        this.mMapView = (MapView) v.findViewById(R.id.map);
        mMapView.showBuiltInScreenButtons(true);
        mMapController = mMapView.getMapController();
        this.mOverlayManager = mMapController.getOverlayManager();
        mMapView.getMapController().getOverlayManager().addOverlay(new CustomOverlay(mMapView.getMapController(), this.mGCListener));

        // Add the layer to the map
        mCustomOverlay = new CustomOverlay(mMapController, mGCListener);
        mOverlayManager.addOverlay(mCustomOverlay);
        return v;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public boolean onItemSelected(String id) {
        final boolean [] res = new boolean[] {true};
        final View v = getView();
        final String prxId = id;
        if (v!=null) {
            boolean post = v.post(new Runnable() {
                @Override
                public void run() {

                    OverlayItem oi = mCustomOverlay.getOverlayItem(prxId);

                    if (oi != null) {
                        mMapController.setPositionAnimationTo(oi.getGeoPoint());
                        mMapController.showBalloon(oi.getBalloonItem());

                        Context c = mMapView.getContext();
                        Animation rotateAnim = AnimationUtils.loadAnimation(c, R.anim.a_ballom);

                        View v = ((AnimatedBalloonItem) (oi.getBalloonItem())).getView().getRootView();
                        if (v!=null){
                            v.startAnimation(rotateAnim);
                        }

                    } else {
                        res[0] = false;
                    }


                }
            });
        }

        return res[0];
    }
}
