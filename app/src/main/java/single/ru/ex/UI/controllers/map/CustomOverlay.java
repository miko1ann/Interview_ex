package single.ru.ex.UI.controllers.map;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.map.GeoCodeListener;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.utils.ScreenPoint;


public class CustomOverlay extends Overlay {

    private GeoCodeListener mGCListener = null;
    private Map<String, OverlayItem> mOIMap = new HashMap<>();

    public CustomOverlay(MapController mapController, GeoCodeListener gcListener) {
        super(mapController);
        mGCListener = gcListener;
    }

    @Override
    public boolean onLongPress(float x, float y) {
        super.onLongPress(x, y);

        if (mGCListener != null) {
            getMapController().getDownloader().getGeoCode(mGCListener, getMapController().getGeoPoint(new ScreenPoint(x, y)));
        } else {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Object another) {
        return 0;
    }


    public void addOverlayItem(String id, OverlayItem oi) {
        super.addOverlayItem(oi);
        mOIMap.put(id, oi);
    }

    public OverlayItem getOverlayItem(String id) {
        OverlayItem  res = null;
        if (mOIMap.containsKey(id)){
             res = mOIMap.get(id);
        }
        return res;
    }

    @Override
    public List<OverlayItem> getOverlayItems() {
        return super.getOverlayItems();
    }

    @Override
    public void clearOverlayItems() {
        super.clearOverlayItems();
        mOIMap.clear();
    }

    @Override
    public void removeOverlayItem(OverlayItem oi) {
        super.removeOverlayItem(oi);
        if (mOIMap.containsKey(oi)) {
            mOIMap.remove(oi);
        }
    }



}

