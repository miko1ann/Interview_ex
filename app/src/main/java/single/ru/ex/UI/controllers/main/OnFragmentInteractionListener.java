package single.ru.ex.UI.controllers.main;


import ru.yandex.yandexmapkit.utils.GeoPoint;

/**
 * This interface must be implemented by activities that contain this
 * fragment to allow an interaction in this fragment to be communicated
 * to the activity and potentially other fragments contained in that
 * activity.
 * <p/>
 * See the Android Training lesson <balloon_red href=
 * "http://developer.android.com/training/basics/fragments/communicating.html"
 * >Communicating with Other Fragments</balloon_red> for more information.
 */

public interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    public void onItemSelected(int position);
    public void onBalloonAdded(GeoPoint geopoint, final String id[], final String test[]);
    public void onYaNavActivated(int pos);
}