package single.ru.ex.UI.controllers.list_items;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;

import java.util.ArrayList;
import java.util.List;

import single.ru.ex.R;
import single.ru.ex.UI.controllers.main.OnFragmentInteractionListener;
import single.ru.ex.UI.data.DataPointsContent;


public class ItemFragment extends Fragment implements AbsListView.OnItemClickListener, View.OnClickListener {

    private OnFragmentInteractionListener mListener;

    private AbsListView mListView = null;
    private ListAdapter mAdapter = null;
    private List<DataPointsContent.DataItem> mViewData = null;
    private int mLastSelectedPos = -1;
    private Button mBtn = null;

    public ItemFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewData = new ArrayList<>();
        mAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1,  mViewData);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item, container, false);

        if (view != null) {
            mListView = (AbsListView) view.findViewById(android.R.id.list);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(this);

            mBtn = (Button) view.findViewById(R.id.button_navigate);
            if (mBtn != null) {
                mBtn.setOnClickListener(this);
            }
        }

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (null != mListener) {
            mListener.onItemSelected(position);
        }
        mLastSelectedPos = position;
        mBtn.setEnabled(true);
    }

    public boolean itemsUpdated(final List<DataPointsContent.DataItem> data) {
        final View v = getView();


        if (v != null) {
            v.post(new Runnable() {
                @Override
                public void run() {
                    mViewData.clear();
                    mViewData.addAll(data);

                    //TODO: Here invoke updating table view

                    mListView.invalidateViews();

                    mLastSelectedPos = -1;
                    mBtn.setEnabled(false);

                }
            });
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        if (null != mListener && mLastSelectedPos != -1 ) {
            mListener.onYaNavActivated(mLastSelectedPos);
        }
    }
}
